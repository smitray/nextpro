import React from 'react';
import PropTypes from 'prop-types';
import { Button as Btn, withStyles } from '@material-ui/core';

const Button = ({
  children,
  variant,
  disabled,
  color,
  size,
  customColor,
  startIcon,
  endIcon,
  href,
  ...other
}) => {
  if (Object.entries(customColor).length !== 0 && customColor.constructor === Object) {
    const ColorButton = withStyles(() => ({
      root: {
        color: `#${customColor.color}`,
        backgroundColor: `#${customColor.bg}`,
        '&:hover': {
          backgroundColor: `#${customColor.hover}`
        }
      }
    }))(Btn);
    return (
      <ColorButton
        variant={variant}
        disabled={disabled}
        size={size}
        href={href}
        startIcon={startIcon}
        endIcon={endIcon}
        {...other}
      >
        {children}
      </ColorButton>
    );
  }
  return (
    <Btn
      variant={variant}
      disabled={disabled}
      color={color}
      size={size}
      href={href}
      startIcon={startIcon}
      endIcon={endIcon}
      {...other}
    >
      {children}
    </Btn>
  );
};

Button.propTypes = {
  /**
   * Variant is defined for different types of buttons
   */
  variant: PropTypes.oneOf(['text', 'outlined', 'contained']),
  /**
   * Disabled is defined if the button is disabled
   */
  disabled: PropTypes.bool,
  /**
   * Color is defined for defining the color of the button
   */
  color: PropTypes.oneOf(['default', 'inherit', 'primary', 'secondary']),
  /**
   * Size is defined for mentioning size of the button
   */
  size: PropTypes.oneOf(['small', 'medium', 'large']),
  /**
   * Custom Color is defined to create a button with custom color.
   * P.S. Please note color property will not take any effect
   */
  customColor: PropTypes.objectOf(PropTypes.string),
  /**
   * Children is defined to show the value of the Button
   */
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node
  ]).isRequired,
  /**
   * Href is defined to assign the button as an anchor tag
   */
  href: PropTypes.string,
  /**
   * End Icon is defined to include icon at the end of the label
   */
  endIcon: PropTypes.node,
  /**
   * Start Icon is defined to include icon at the beginning of the button
   */
  startIcon: PropTypes.node
};

Button.defaultProps = {
  variant: 'contained',
  disabled: false,
  color: 'primary',
  size: 'medium',
  customColor: {},
  href: null,
  startIcon: '',
  endIcon: ''
};

export default Button;
