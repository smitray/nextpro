import React from 'react';
import PropTypes from 'prop-types';
import { IconButton as IcBtn, withStyles } from '@material-ui/core';

const IconButton = ({
  children,
  disabled,
  color,
  size,
  customColor,
  ...other
}) => {
  if (Object.entries(customColor).length !== 0 && customColor.constructor === Object) {
    const ColorButton = withStyles(() => ({
      root: {
        color: `#${customColor.color}`,
        '&:hover': {
          backgroundColor: `${customColor.hover}`
        }
      }
    }))(IcBtn);
    return (
      <ColorButton
        disabled={disabled}
        size={size}
        {...other}
      >
        {children}
      </ColorButton>
    );
  }
  return (
    <IcBtn
      disabled={disabled}
      color={color}
      size={size}
      {...other}
    >
      {children}
    </IcBtn>
  );
};

IconButton.propTypes = {
  /**
   * Disabled is defined if the button is disabled
   */
  disabled: PropTypes.bool,
  /**
   * Color is defined for defining the color of the button
   */
  color: PropTypes.oneOf(['default', 'inherit', 'primary', 'secondary']),
  /**
   * Size is defined for mentioning size of the button
   */
  size: PropTypes.oneOf(['small', 'medium']),
  /**
   * Custom Color is defined to create a button with custom color.
   * P.S. Please note color property will not take any effect
   */
  customColor: PropTypes.objectOf(PropTypes.string),
  /**
   * Children is defined to show the value of the Button
   */
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node
  ]).isRequired
};

IconButton.defaultProps = {
  disabled: false,
  color: 'primary',
  size: 'medium',
  customColor: {}
};

export default IconButton;
