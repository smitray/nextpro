import React from 'react';
import PropTypes from 'prop-types';
import Button from '../Button';
import IconButton from '../IconButton';

const UploadButton = ({
  icon,
  fileType,
  multiple,
  children,
  ...other
}) => (
  <div>
    <input
      accept={fileType}
      id="file-input"
      multiple={multiple}
      type="file"
      style={{ display: 'none' }}
    />
    <label htmlFor="file-input">
      {icon ? (
        <IconButton component="span" {...other}>
          {children}
        </IconButton>
      ) : (
        <Button component="span" {...other}>
          {children}
        </Button>
      )}
    </label>
  </div>
);

UploadButton.propTypes = {
  /**
   * Icon is defined to add icon
   */
  icon: PropTypes.bool,
  /**
   * Filetype is defined to describe mimetype
   */
  fileType: PropTypes.string.isRequired,
  /**
   * Multiple is defined to enable multi select
   */
  multiple: PropTypes.bool,
  /**
   * Children is defined to show the value of the Button
   */
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node
  ]).isRequired
};

UploadButton.defaultProps = {
  icon: false,
  multiple: false
};

export default UploadButton;
